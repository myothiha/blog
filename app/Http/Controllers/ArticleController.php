<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\ArticleRequest;

use App\Article;
use Carbon\Carbon;
use Auth;

class ArticleController extends Controller
{
    public function index() {
        
        // $articles = Article::latest('published_at')->where('published_at', '<=', Carbon::now())->get();
        // dd($articles->toArray());
        
        // dd(Article::find(1)->user->toArray());
        
        $articles = Article::latest('published_at')->published()->get();
        return view('articles.index', [
            'articles' => $articles
        ]);
    }
    
    public function show(Article $article) {
        return view('articles.show', [
            'article' => $article
        ]);
    }
    
    public function create() {
        return view('articles.create');
    }
    
    public function store(ArticleRequest $request) {
        
        // Article::create( $request->all());
        
        Auth::user()->articles()
                    ->save( new Article($request->all()) );
        return redirect('article');
    }
    
    public function edit(Article $article) {

        return view('articles.edit', [
            'article' => $article,    
        ]);
    }
    
    public function update(Article $article, ArticleRequest $request) {
        $article->update($request->all());
        return redirect('article');
    }
}
