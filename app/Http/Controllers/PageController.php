<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    
    public function about() {
        $about = "<p style='color:red;'>Myo Thiha</p>";
        
        return view('pages.about', [
            'name' => $about 
        ]);
    }
}
