<div class="form-group">
        {!! Form::label('title', 'Title: ') !!}
        {!! Form::text('title', null, [
            'class' => 'form-control',
            'id'    => 'title',
            'placeholder' => 'Title'
        ]) !!}
    </div>
    
    <div class="form-group">
        {!! Form::label('title', 'Body: ') !!}
        {!! Form::textarea('body', null, [
            'class' => 'form-control',
            'id'    => 'body',
            'placeholder' => 'Body'
        ]) !!}
    </div>
    
    <div class="form-group">
        {!! Form::label('published_at', 'Published On: ') !!}
        {!! Form::input('date', 'published_at', date('Y-m-d'), [
            'class' => 'form-control',
            'id'    => 'published_at',
            'placeholder' => 'Body'
        ]) !!}
    </div>
    
    <div class="form-group">
        {!! Form::submit($submitButtonText, [
            'class' => 'btn btn-primary form-control',
            'id'    => 'submit',
        ]) !!}
    </div>