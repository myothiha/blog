@extends('layouts.app')

@section('content')

<h3>All Articles</h3>
    <h2> {{ $article->title }} </h2>
    
    <p> {{ $article->body }} </p>
    
    <p> {{ $article->published_date }}</p>
@stop