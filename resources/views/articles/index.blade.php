@extends('layouts.app')

@section('content')

<a href="article/create" class='btn btn-primary' >Add New Article</a>

<h3>All Articles</h3>

<hr />
@if ( $articles->isNotEmpty() )
    @foreach( $articles as $article )
    
    <h3>
        <a href="article/{{ $article->id }}">
            {{ $article->title }}
        </a>
        
        <a href={{ action('ArticleController@show', $article->id) }} >
            {{ $article->title }}
        </a>
        
        <a href={{ url('article', $article->id) }} >
            {{ $article->title }}
        </a>
        
        <a href="article/{{ $article->id }}/edit" class="btn btn-primary"> Edit </a>
    </h3>
    
    <p> {{ $article->body }} </p>
    
    <h5> <b> Published by {{ $article->user->name }} on {{ $article->published_at->diffForHumans() }} </b> </h5>
    <br />
    
    @endforeach
@else
 <p>There is no article. Please Create one today.</p>
@endif
@stop