@extends('layouts.app')

@section('content')

<h3>Edit {{ $article->title }}</h3>

    <hr />
    
    @include('errors.list')
    
    {!! Form::model($article, ['method' => 'PATCH', 'action'=>['ArticleController@update', $article->id]]) !!}
        
        @include('articles.partials.form', [
            'submitButtonText' => 'Update Article'
        ])

    {!! Form::close() !!}
@stop