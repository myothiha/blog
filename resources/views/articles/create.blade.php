@extends('layouts.app')

@section('content')

<h3>Create New Article</h3>

    <hr />
    
    @include('errors.list')
    
    {!! Form::open(['action'=>'ArticleController@store']) !!}
    
        @include('articles.partials.form', [
            'submitButtonText' => 'Add Article'
        ])
    
    {!! Form::close() !!}
@stop