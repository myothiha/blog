<!DOCTYPE html>
<html>
    <head>
        <title>My Own Blog</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" type="text/css" />
    </head>
    
    <body>
        
        <div class='container'>
            @yield('content')
        </div>
    </body>
</html>